package com.tsc.skuschenko.tm.command.user;

import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.endpoint.User;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.exception.entity.user.UserNotFoundException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    private static final String DESCRIPTION = "view profile of current user";

    @NotNull
    private static final String NAME = "view-user-profile";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        @Nullable final Session session = serviceLocator.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        @Nullable final User user =
                Optional.ofNullable(serviceLocator.getUserEndpoint().getUser())
                        .orElseThrow(UserNotFoundException::new);
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole());
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
