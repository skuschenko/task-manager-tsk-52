package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IServiceLocator {

    void exit();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    DataEndpoint getDataEndpoint();

    @NotNull
    ProjectEndpoint getProjectEndpoint();

    @NotNull
    IPropertyService getPropertyService();

    @Nullable Session getSession();

    void setSession(@Nullable Session session);

    @NotNull
    SessionEndpoint getSessionEndpoint();

    @NotNull
    TaskEndpoint getTaskEndpoint();

    @NotNull
    UserEndpoint getUserEndpoint();

}
