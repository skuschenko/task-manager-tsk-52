package com.tsc.skuschenko.tm.api.repository.model;

import com.tsc.skuschenko.tm.model.User;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IUserRepository extends IAbstractRepository<User> {

    void clear();

    @Nullable List<User> findAll();

    @Nullable
    @SneakyThrows
    User findByEmail(@NotNull String email);

    @Nullable
    User findById(@NotNull String id);

    @Nullable
    @SneakyThrows
    User findByLogin(@NotNull String login);

    @Nullable
    User getReference(@NotNull String id);

    @NotNull
    @SneakyThrows
    User lockUserByLogin(@NotNull String login);

    @Nullable
    @SneakyThrows
    User removeByLogin(@NotNull String login);

    void removeOneById(@NotNull String id);

    @NotNull
    @SneakyThrows
    User setPassword(
            @NotNull String userId, @NotNull String password
    );

    @NotNull
    @SneakyThrows
    User unlockUserByLogin(@NotNull String login);
}
